 # acqsk:account-token-expiration
Client/server account token expiration plugin for Meteor framework inpired by zuuk:stale-session.

### Configure plugin settings
```
var settings = {
    // Client settings
    public: {
        SESSION_EXPIRATION: {
            expirationInterval: 300000 // Token expiration interval, default 5min
            expireOnCloseWindow: true // Logout user when browser window/tab closed, default true,
            heartBeatInterval: 30000 // Client->server comm interval for update expiration on server, default 30s,
            activityEvents: 'click mousemove keydown DOMMouseScroll mousewheel mousedown touchstart touchmove focus' // Events which mean, that client is active
        }
    }

    // Server settings
    SESSION_EXPIRATION: {
        activeLogs: false, // Enable log events in server console, default false
        expirationInterval: 300000 // Token expiration interval, default 5min (same as client side),
        heartBeatInterval: 60000 // Server clear expired tokens interval, default 1min
    }
}
```

### Notice
Logout on close browser window was temporary deactivated, because I don't know how, to detect, if browser windows is going close or refresh.