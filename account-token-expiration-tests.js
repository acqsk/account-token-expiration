// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by meteor-session-expiration.js.
import { name as packageName } from "meteor/acqsk:meteor-session-expiration";

// Write your tests here!
// Here is an example.
Tinytest.add('meteor-session-expiration - example', function (test) {
  test.equal(packageName, "meteor-session-expiration");
});
