import createActivityDetector from 'activity-detector';
import Settings from './settings';

let _lastActivity = new Date();
let _isActive = true;
let _settings = Settings.getSettings();
let _interval = null;


/**
 * Check expiration interval
 * Update user token timestamp or logout
 */
function _activeTimer() {
    let prevInterval = _settings.heartBeatInterval;
    _settings = Settings.getSettings();

    // Re-create activeTimer, if interval was changed
    if (prevInterval !== _settings.heartBeatInterval) {
        clearInterval(_interval);
        _interval = setInterval(_activeTimer, _settings.heartBeatInterval);
    }

    if (_settings.enabled) {
        // If user is loggedIn
        if (_settings.expirationInterval && Meteor.user()) {
            let mseconds = Math.floor(new Date().getTime() - _lastActivity.getTime());
            if (mseconds > 10000 && _isActive) {
                _isActive = false;
            }
            if (mseconds >= _settings.expirationInterval) {
                if (_isActive) {
                    let token = Meteor._localStorage.getItem('Meteor.loginToken');
                    if (token) {
                        Meteor.call('accountTokup', {'token': token});
                    }
                }
            } else {
                Meteor.logout();
            }
        } else {
            if (_isActive)
                _isActive = false;
        }
    }
}

/**
 * Start hearth
 * @type {number}
 * @private
 */
_interval = setInterval(_activeTimer, _settings.heartBeatInterval);


/**
 * Meteor logout when close tab/window
 */
window.addEventListener('onbeforeunload', function () {
    if (Meteor.user() && _settings.expireOnCloseWindow) {
        Meteor.logout();
    }
});

/**
 * Refresh last activity
 */
if (_settings && _settings.activityEvents) {
    Array.prototype.forEach.call(_settings.activityEvents.split(' '), eventName => {
        if (typeof document.body["on" + eventName] !== "undefined") {
            window.addEventListener(eventName, e => {
                _lastActivity = new Date();
                _isActive = true;
            })
        }
    })
}