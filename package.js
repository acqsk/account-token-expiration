Package.describe({
  name: 'acqsk:account-token-expiration',
  version: '0.6.1',
  // Brief, one-line summary of the package.
  summary: 'Client/server account token expiration plugin for Meteor framework inpired by zuuk:stale-session',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/acqsk/meteor-session-expiration',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.11.1');
  api.use('ecmascript');
  api.use('accounts-base@1.7.0', ['client','server']);
  api.addFiles('client/client.js', 'client');
  api.addFiles('server/server.js', 'server');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('accounts-base@1.7.0', ['client','server']);
  api.use('tinytest');
  api.use('acqsk:meteor-session-expiration');
  api.mainModule('account-token-expiration-tests.js');
});

Npm.depends({
  'activity-detector': '3.0.0',
  'colors': '1.3.2'
});
