import 'colors';
import Settings from './settings';

let _settings = Settings.getSettings();
let _interval = null;
let _resetInterval = false;

const log = function (msg, color) {
    if (_settings.activeLogs) {
        msg = '[' + new Date().toISOString() + '] ' + msg;
        if (color) {
            console.log(msg[color]);
        } else {
            console.log(msg);
        }
    }
};

const logError = function (err) {
    const color = 'red';
    if (err && err.message != null) {
        console.log(err.message[color]);
    } else {
        console.log(err[color])
    }
};

const logRed = function () {
    log(Array.prototype.slice.call(arguments).join(' '), 'red');
};
const logGrey = function () {
    log(Array.prototype.slice.call(arguments).join(' '), 'grey');
};
const logGreen = function () {
    log(Array.prototype.slice.call(arguments).join(' '), 'green');
};
const logYellow = function () {
    log(Array.prototype.slice.call(arguments).join(' '), 'yellow');
};


function _activeTimer() {
    let prevInterval = _settings.heartBeatInterval;
    _settings = Settings.getSettings();

    // Re-create activeTimer, if interval was changed
    if (prevInterval !== _settings.heartBeatInterval) {
        Meteor.clearInterval(_interval);
        _interval = Meteor.setInterval(_activeTimer, _settings.heartBeatInterval);
    } else {
        if (_settings.enabled) {
            const overdueTimestamp = new Date(new Date() - _settings.expirationInterval);
            let data = Meteor.users.find({'services.resume.loginTokens.when': {$exists: true}}, {
                fields: {
                    '_id': 1,
                    'services.resume.loginTokens': 1
                }
            }).map(u => {
                if (u.services && u.services.resume && u.services.resume.loginTokens) {
                    u.services.resume.loginTokens.forEach((t, i) => {
                        if (t.when <= overdueTimestamp) {
                            u.services.resume.loginTokens.splice(i, 1);
                            u.updated = true;
                        }
                    });
                }
                return u;
            });
            //@TODO Is it possible to make this query more effective (without iteration user-by-user)?
            if (data) {
                data.forEach(item => {
                    try {
                        if (item.updated) {
                            Meteor.users.update({'_id': item._id}, {
                                $set: {
                                    'services.resume.loginTokens': item.services.resume.loginTokens
                                }
                            });
                            logGreen(`Removed token for userId ${item._id}`);
                        }
                    } catch (err) {
                        logError(err);
                    }
                });
            }

            logYellow('=== Token heartBeat ===');
        }

    }
}

_interval = Meteor.setInterval(_activeTimer, _settings.heartBeatInterval);

/**
 * Here update the where timestamp in user loginTokens
 */
Meteor.methods({
    accountTokup: function (options) {
        if (_settings.enabled) {
            //@TODO How to know token directly from request (I dont want to send it in options...)?
            if (!Meteor.userId() || !options.token) {
                return;
            }
            let hashedToken = Accounts._hashLoginToken(options.token);
            let user = Meteor.users.findOne({'_id': Meteor.userId()}, {
                fields: {
                    '_id': 1,
                    'services.resume.loginTokens': 1
                }
            });
            if (!user) {
                return;
            }

            if (user.services && user.services.resume && user.services.resume.loginTokens) {
                let tokens = user.services.resume.loginTokens;
                tokens = tokens.map(t => {
                    if (t.hashedToken === hashedToken) {
                        t.when = new Date();
                    }
                    return t;
                });

                try {
                    Meteor.users.update({'_id': user._id}, {$set: {'services.resume.loginTokens': tokens}});
                    logGreen(`=== updatedAccountTokup === ${user._id}`);
                } catch (err) {
                    logError(err);
                }
            }
            logYellow(`=== accountTokup === ${user._id}`);
        }
    }
});